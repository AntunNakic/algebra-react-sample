import _ from "lodash";
import React, { Component } from "react";
import MazeDetail from "../detailQuery";
import VideoDetail from "./video_details";

// Create new component. This component should produce
// some HTML
class Details extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedMovie: null,
    };
  }

  componentDidMount() {
    let id = location.pathname.slice(location.pathname.lastIndexOf("/") + 1);

    MazeDetail({ id: id }, (movie) => {
      this.setState({
        selectedMovie: movie,
      });
    });
  }
  render() {
    return (
      <div>
        <VideoDetail movie={this.state.selectedMovie} />
      </div>
    );
  }
}

export default Details;
