import React from "react";

const VideoList = (props) => {
  if (props.videos.length) {
    const videoItems = props.videos.map((movie) => {
      return (
        <a
          href={"/show/" + movie.show.id}
          className="list-group-item list-group-item-action"
        >
          {movie.show.name}
        </a>
      );
    });
    return (
      <div
        className="col-md-6"
        style={{ textAlign: "center", alignSelf: "center" }}
      >
        <h3>Rezultati pretrage:</h3>
        <div className="list-group" style={{ marginTop: "20px" }}>
          {videoItems}
        </div>
      </div>
    );
  } else {
    return (
      <div style={{ textAlign: "center", height: "300px" }}>
        <h3>Rezultati pretrage:</h3>
      </div>
    );
  }
};

export default VideoList;

//href={movie.id} za dobiti lokalno
