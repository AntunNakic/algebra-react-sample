import React from "react";

const VideoDetail = ({ movie }) => {
  if (!movie) {
    return <div>Loading...</div>;
  }

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "space-between",
        backgroundColor: "aliceblue",
        marginTop: "20px",
        padding: "20px",
      }}
    >
      <div style={{ flexBasis: "30%" }}>
        <img src={movie.image.original} style={{ maxWidth: "100%" }} />
      </div>
      <div style={{ flexBasis: "65%" }}>
        <h2 style={{ textAlign: "center" }}>{movie.name}</h2>

        <hr />
        <strong>Genres:</strong>
        <ul>
          {movie.genres.map((x) => {
            return <li> {x} </li>;
          })}
        </ul>
        <p>
          <strong>Premiered:</strong> {movie.premiered}
        </p>
        <p>
          <strong>Duration:</strong> {movie.runtime} minutes
        </p>
        <hr />
        <p>
          <strong>Summary:</strong>
        </p>

        <div dangerouslySetInnerHTML={{ __html: movie.summary }}></div>
      </div>
    </div>
  );
};

export default VideoDetail;
