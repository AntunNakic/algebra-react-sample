import _ from "lodash";
import React, { Component } from "react";
import MazeSearch from "../searchQuery";
import SearchBar from "./search_bar";
import VideoList from "./video_list";

// Create new component. This component should produce
// some HTML
class Search extends Component {
  constructor(props) {
    super(props);

    this.state = {
      movies: [],
    };
  }

  render() {
    return (
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "space-between",
          backgroundColor: "aliceblue",
          marginTop: "20px",
          padding: "20px",
        }}
      >
        <h3 style={{ textAlign: "center" }}>Pretraži film:</h3>
        <SearchBar
          onSearchTermChange={(term) =>
            MazeSearch({ term: term }, (movies) => {
              this.setState({
                movies: movies,
              });
            })
          }
        />

        <VideoList
          onVideoSelect={(selectedMovie) => this.setState({ selectedMovie })}
          videos={this.state.movies}
        />
      </div>
    );
  }
}

export default Search;
