var axios = require("axios");

var ROOT_URL = "http://api.tvmaze.com/search/shows";

module.exports = function (options, callback) {
  if (!options.term) {
    throw new Error("Niste odabrali termin za pretragu");
  }

  var params = {
    q: options.term,
  };

  axios
    .get(ROOT_URL, { params: params })
    .then(function (response) {
      if (callback) {
        callback(response.data);
      }
    })
    .catch(function (error) {
      console.error(error);
    });
};
