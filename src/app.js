import React from "react";
import { Switch, Route } from "react-router-dom";
import Search from "./components/search";
import Details from "./components/details";

const App = (props) => {
  window.scrollTo(0, 0);
  return (
    <main>
      <Switch>
        <Route exact path="/" render={() => <Search />} />
        <Route exact path="/search" render={() => <Search />} />
        <Route path="/show/" render={() => <Details />} />
      </Switch>
    </main>
  );
};

export default App;
