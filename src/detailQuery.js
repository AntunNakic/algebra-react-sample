var axios = require("axios");

var ROOT_URL = "http://api.tvmaze.com/shows/";

module.exports = function (options, callback) {
  if (!options.id) {
    throw new Error("Odaberite id filma");
  }

  ROOT_URL += options.id;

  axios
    .get(ROOT_URL)
    .then(function (response) {
      if (callback) {
        callback(response.data);
      }
    })
    .catch(function (error) {
      console.error(error);
    });
};
